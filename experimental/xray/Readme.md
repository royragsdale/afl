# Benchmark with LLVM XRay

LLVM XRay allows the collection of function call granularity timing information.

This directory contains profiles for instrumenting specific functions in
`afl-fuzz`.
